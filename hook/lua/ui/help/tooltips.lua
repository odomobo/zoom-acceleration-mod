do
    Tooltips.options_gui_zoom_speed_large = {
        title = "Zoom Acceleration (Coarse)",
        description = "The initial acceleration value, when first zooming to a new zoom level. Default: 5",
    }
    Tooltips.options_gui_zoom_speed_small = {
        title = "Zoom Acceleration (Fine)",
        description = "The ending (slower) acceleration value, when nearly zoomed in to the requested zoom level. Default: 5",
    }
end